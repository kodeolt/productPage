// Task runner - Gulp
var gulp = require('gulp');

// Gulp prapletimas SASS konvertavimui i CSS
var sass = require('gulp-sass');

// For minifying files
var minify = require('gulp-minify');
var cleanCSS = require('gulp-clean-css');

// Renaming files
var rename = require('gulp-rename');

// Autoprefixer - uzdeda automatiskai reikiamus prefixus
var autoprefixer = require('gulp-autoprefixer');

// Paveiksliuku optimizavimui
var imagemin = require('gulp-imagemin');
var imgJpgRecompress = require('imagemin-jpeg-recompress');

// Livereload + device sync 
var browserSync = require('browser-sync').create();


/*
	Task for compiling SCSS to CSS
	Also adds prefixes
	Compiles from src to dist folder
 */
gulp.task('styles', function() {
	gulp.src('src/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer())
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest('dist/css/'))
		.pipe(browserSync.stream())
});


/*
	Task to minify js
	Compiles from src to dist folder
 */
gulp.task('scripts', function() {
	gulp.src('src/js/**/*.js')
		.pipe(minify({
			ext: {
				min: '.min.js'
			},
			noSource: true
		}))
		.pipe(gulp.dest('dist/js/'))
		.pipe(browserSync.stream())
});

/*
	Task to minimize images
	Compiles from src to dist folder
 */
gulp.task('images', function() {
	gulp.src('src/images/*')
		.pipe(imagemin([
			imagemin.gifsicle(),
			imgJpgRecompress({
				loops: 4,
				min: 50,
				max: 95,
				quality: 'medium'
			}),
			imagemin.optipng(),
			imagemin.svgo()
		]))
		.pipe(gulp.dest('dist/images'))
});


/*
	Server task for running browsersync and other tasks
	also watches for changes in sass, js and html files
 */
gulp.task('serve', ['styles', 'scripts', 'images'], function() {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});
	gulp.watch('src/sass/**/*.scss', ['styles']);
	gulp.watch('src/js/**/*.js', ['scripts']);
	gulp.watch('**/*.html').on('change', browserSync.reload);

});

/*
	Default task. When running gulp - launches server task
 */

gulp.task('default', ['serve']);
